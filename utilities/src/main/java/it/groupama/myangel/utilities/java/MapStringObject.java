package it.groupama.myangel.utilities.java;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Francesco in 05/01/17.
 */

public class MapStringObject extends LinkedHashMap<String, Object> {

    public MapStringObject() {
    }

    public MapStringObject(int initialCapacity) {
        super(initialCapacity);
    }

    public MapStringObject(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public MapStringObject(int initialCapacity, float loadFactor, boolean accessOrder) {
        super(initialCapacity, loadFactor, accessOrder);
    }

    public MapStringObject(Map<? extends String, ?> map) {
        super(map);
    }

    @SuppressWarnings("unchecked")
    public <T> T getValue(String key) {
        Object o = get(key);

        if (o != null) {
            return (T) o;
        }

        return null;
    }

    public byte getByte(String key) {
        Object value = get(key);

        if (value != null) {
            return (byte) value;
        }

        return 0;
    }

    public short getShort(String key) {
        Object value = get(key);

        if (value != null) {
            return (short) value;
        }

        return 0;
    }

    public int getInt(String key) {
        Object value = get(key);

        if (value != null) {
            return (int) value;
        }

        return 0;
    }

    public long getLong(String key) {
        Object value = get(key);

        if (value != null) {
            return (long) value;
        }

        return 0;
    }

    public float getFloat(String key) {
        Object value = get(key);

        if (value != null) {
            return (float) value;
        }

        return 0.0f;
    }

    public double getDouble(String key) {
        Object value = get(key);

        if (value != null) {
            return (double) value;
        }

        return 0.0d;
    }


    public char getChar(String key) {
        Object value = get(key);

        if (value != null) {
            return (char) value;
        }

        return '\u0000';
    }

    public boolean getBoolean(String key) {
        Object value = get(key);
        return value != null && (boolean) value;
    }

    public static MapStringObject getInstance(String key, Object value) {
        final MapStringObject mapStringObject = new MapStringObject();
        mapStringObject.put(key, value);
        return mapStringObject;
    }

}