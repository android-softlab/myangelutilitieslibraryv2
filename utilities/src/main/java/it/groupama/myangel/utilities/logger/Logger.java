package it.groupama.myangel.utilities.logger;

import android.os.Handler;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Serializable;


/*
*
* La classe Logger contiene una serie di metodi utili alla gestione del log:
*
* Esempio di utilizzo:
* 1. Creare un istanza della classe Logger:
*    private Logger logger;
* 2. Inizializzare la variabile creata:
*    con questa istruzione viene assegnato un valore alla variabile 'tag:
*    logger = Logger.getIntance(getClass().getSimpleName());
* 3. a questo punto, quanto il programmatore lo ritiene necessario, può stampare il log:
*    logger.debug("messaggio", parametri-opzionale-);
* 4. l'output è del tipo "tag: messaggio + parametri"
*
*
* */
@SuppressWarnings({"StringBufferReplaceableByString", "WeakerAccess"})
public class Logger implements Serializable {

    private static final int TAG_LIMIT = 23;

    private String nameManager;
    private final String tag;

    public static Logger getIntance(String tag) {
        return new Logger(tag);
    }

    /**
     * Richiama il costruttore della classe Logger, inizializza le laviabili tag e nameManager
     *
     * @param clazz
     * @return
     */
    public static Logger getIntance(Class<?> clazz) {
        return new Logger(clazz.getSimpleName());
    }

    /**
     *
     * Richiama il costruttore della classe Logger, inizializza le laviabili tag e nameManager
     *
     * @param tag
     * @param nameManager
     * @return
     */
    public static Logger getIntance(String tag, String nameManager) {
        return new Logger(tag, nameManager);
    }

    /**
     *
     * Costruttori della classe Logger,
     * attenzione: sono privati, vengono solo richiamati da getInstance
     *  la variabile tag non può essere più lunga di "TAG_LIMIT"
     *
     * @param tag
     */



    private Logger(String tag) {
        this(tag, "");
    }

    private Logger(String tag, String nameManager) {
        this.nameManager = nameManager;
        this.tag = getTag(tag);
    }

    /**
     *
     * Controllo sulla lunghezza della variabile tag,
     * La stringa TAG non può essere più lunga di TAG_LIMIT
     *
     * @param tag
     * @return
     */

    private String getTag(String tag) {
        if (tag.length() > TAG_LIMIT) {
            tag = tag.substring(0, TAG_LIMIT);
        }

        return tag;
    }


    /**
     * I metodi seguenti chiamano la classe Log di android, per la stampa del log
     * Descriviamo il cocmportamento in base ai parametri che ricevono:
     * --> Object... objects : prima di stampare il messaggio, viene creata una stringa concatenando tutti gli objects
     * --> String msg : viene stampata direttamente la stringa
     * --> String msg, Object... objects: formatta il testo secondo la logica: "Se la stringa msg contiene {} allora queste devono essere sostituite dalla stringa objects"
     * --> String msg, Type type, Object... objects:: aggiunge la possibilità di scegliere se visualizzare il log sulla cosole, se stamparlo su file, o entrambi
     *
     * @param objects
     */



    public void debug(Object... objects) {
        Log.d(tag, getMessage(objects));
    }

    public void debug(String msg) {
        Log.d(tag, getMessage(msg));
    }

    public void debug(String msg, Object... objects) {
        Log.d(tag, getMessage(msg, objects));
    }

    public void debug(String msg, Type type, Object... objects) {
        String message = getMessage(msg, objects);
        switch (type) {
            case FILE: {
                writeToFile(message);
                break;
            }
            case CONSOLE: {
                Log.d(tag, message);
                break;
            }
            case ALL: {
                Log.d(tag, message);
                writeToFile(message);
                break;
            }
        }
    }

    public void debugFile(String msg, Object... objects) {
        writeToFile(getMessage(msg, objects));
    }

    public void warn(String msg) {
        Log.w(tag, getMessage(msg));
    }

    public void warn(String msg, Throwable e) {
        Log.w(tag, getMessage(msg), e);
    }

    public void warn(Throwable e) {
        Log.w(tag, getMessage(e.getMessage()), e);
    }

    public void warnNoMessage(Throwable e) {
        Log.w(tag, e);
    }

    public void error(String msgError) {
        Log.e(tag, getMessage(msgError));
    }

    public void error(Object... objects) {
        Log.e(tag, getMessage(objects));
    }

    public void error(Throwable e) {
        Log.e(tag, getMessage(e.getMessage()), e);
    }

    public void errorMessage(Throwable e) {
        Log.e(tag, getMessage(new StringBuilder(e.getClass().getName()).append(" - ").append(e.getMessage())));
    }


    public void error(String msg, Throwable e) {
        Log.e(tag, getMessage(msg), e);
    }

    public void error(String msg, Object... objects) {
        Log.e(tag, getMessage(msg, objects));
    }

    public void mock(String message) {
        error(new MockException(message));
    }

    public void mock() {
        error(new MockException());
    }

    public String getNameManager() {
        return nameManager != null ? nameManager : "";
    }

    private String getMessage(Object... objects) {
        return getNameManager() + " - " + getString(objects);
    }

    private String getString(Object... objects) {
        try {
            StringBuilder builder = new StringBuilder();
            if (objects != null) {
                for (Object o : objects) {
                    builder.append(o).append(" ");
                }

                if (builder.lastIndexOf(" ") != -1) {
                    builder.replace(builder.lastIndexOf(" "), builder.length(), "");
                }

            }
            return builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private String getPrefixMessage() {
        return getNameManager() + " - ";
    }

    private String getMessage(String msg) {
        return new StringBuilder(getPrefixMessage()).append((msg != null ? msg : "")).toString();
    }

    private static final String MARKER = "{}";

    private String getMessage(String msg, Object... objects) {
        StringBuilder message;
        if (msg.contains(MARKER)) {
            message = new StringBuilder(getPrefixMessage()).append(msg);
            for (Object object : objects) {
                int indexOf = message.indexOf(MARKER);
                if (indexOf == -1) {
                    break;
                }
                message.replace(indexOf, indexOf + MARKER.length(), getString(object));
            }

        } else {
            message = new StringBuilder(getMessage(msg)).append(getString(objects));
        }

        return message.toString();

//        return new StringBuilder(getMessage(msg)).append(getString(objects)).toString();
    }

    private void writeToFile(final String value) {
        new Handler().post(new Runnable() {

            @Override
            public void run() {
                try {

                    final String localPath = "";
                    final String filename = "";

                    BufferedWriter bos = new BufferedWriter(new FileWriter(
                            localPath + "/" + filename));
                    bos.write(value);
                    bos.flush();
                    bos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private class MockException extends RuntimeException {


        public MockException() {
            this("Mock exception");
        }

        public MockException(String detailMessage) {
            super(detailMessage);
        }

        public MockException(String detailMessage, Throwable throwable) {
            super(detailMessage, throwable);
        }

        public MockException(Throwable throwable) {
            super(throwable);
        }
    }

    public enum Type {
        CONSOLE, FILE, ALL
    }


    @Override
    public String toString() {
        return new StringBuilder(Integer.toHexString(hashCode())).append('@').append(tag).toString();
    }
}