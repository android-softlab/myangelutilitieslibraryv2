package it.groupama.myangel.utilities.sharedUtils;

/**
 * Created by softlab on 14/07/2017.
 */


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.GsonBuilder;
import com.google.gson.internal.$Gson$Types;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francesco in 20/06/17.
 */


/**
*  GUIDA:
*   viene creato un file con il nome "context.getPackageName()"
*   esempio di utilizzo : SharedUtils.saveData(SharedUtilsTest.this,"key","value");
 *   La classe prevede sia metodi di set, per salvare i dati sul file,
 *   che metodi di get per recuperare i dati.
*  */
public abstract class SharedUtils {


    public static void saveData(final Context context, final String key, final String value) {
        SharedPreferences sharedPreferences = getInstance(context);
        sharedPreferences.edit().putString(key, value).apply();
    }

    public static void saveData(final Context context, final String key, final boolean value) {
        SharedPreferences sharedPreferences = getInstance(context);
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public static void saveData(final Context context, final String key, final int value) {
        SharedPreferences sharedPreferences = getInstance(context);
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public static void saveData(final Context context, final String key, final long value) {
        SharedPreferences sharedPreferences = getInstance(context);
        sharedPreferences.edit().putLong(key, value).apply();
    }

    public static void saveData(final Context context, final String key, final float value) {
        SharedPreferences sharedPreferences = getInstance(context);
        sharedPreferences.edit().putFloat(key, value).apply();
    }

    /**
     * Se passi un object, la key diventa il nome della classe.
     * per richiamarla utilizzare il .class
     */
    public static void saveData(final Context context, final  Object object) {
        SharedPreferences sharedPreferences = getInstance(context);
        final String json = new GsonBuilder().create().toJson(object);

        sharedPreferences.edit().putString(object.getClass().getSimpleName(), json).apply();
    }

    public static void saveData(final Context context, final String key, final List list) {
        SharedPreferences sharedPreferences = getInstance(context);
        final String json = new GsonBuilder().create().toJson(list);

        sharedPreferences.edit().putString(key, json).apply();
    }

    public static String getString(final Context context, final String key) {
        SharedPreferences sharedPreferences = getInstance(context);
        return sharedPreferences.getString(key, "");
    }

    public static boolean getBoolean(final Context context, final String key) {
        SharedPreferences sharedPreferences = getInstance(context);
        return sharedPreferences.getBoolean(key, false);
    }

    public static int getInt(final Context context, final String key) {
        SharedPreferences sharedPreferences = getInstance(context);
        return sharedPreferences.getInt(key, -1);
    }

    public static long getLong(final Context context, final String key) {
        SharedPreferences sharedPreferences = getInstance(context);
        return sharedPreferences.getLong(key, -1);
    }

    public static float getFloat(final Context context, final String key) {
        SharedPreferences sharedPreferences = getInstance(context);
        return sharedPreferences.getFloat(key, -1);
    }

    public static <T> T getObject(final Context context, final Class<T> returnClass) {
        SharedPreferences sharedPreferences = getInstance(context);

        final String json = sharedPreferences.getString(returnClass.getSimpleName(), null);
        if (json == null) {
            return null;
        }
        return new GsonBuilder().create().fromJson(json, returnClass);
    }


    public static <T> List<T> getListPreferences(final Context context, final String key, final Class<T> returnClass) {
        SharedPreferences settings = getInstance(context);
        String json = settings.getString(key, "[]");
        ParameterizedType parameterizedType = $Gson$Types.newParameterizedTypeWithOwner(null, ArrayList.class, returnClass);

        return new GsonBuilder().create().fromJson(json, parameterizedType);
    }


    public static void removeObject(final Context context, final Class<?> objectClass) {
        getInstance(context).edit().remove(objectClass.getSimpleName()).apply();
    }

    private static SharedPreferences getInstance(final Context context) {
        return context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }
}