package it.groupama.myangel.utilities.validator;


import it.groupama.myangel.utilities.validator.exception.ValidationException;

/**
 * Created by Francesco in 23/06/17.
 */


/**
 * è il validetor vero e proprio,
 * La classe che lo implementa deve fare l'override del metodo 'validate',
 * può quindi definire le regole per accettare la stringa value.
 * */

public interface Validator {

    void validate(String value) throws ValidationException;
}
