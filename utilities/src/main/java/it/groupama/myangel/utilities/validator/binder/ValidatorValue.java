package it.groupama.myangel.utilities.validator.binder;

import it.groupama.myangel.utilities.validator.exception.ValidationException;

/**
 * Created by Francesco in 03/07/17.
 */


/**
 * Non si occupa davvero di validare una oggetto.
 * ad esempio viene esteso da un editText personalizzata, dove vogliamo gestire
 * la validazione dell'input
 * Se una classe la implementa, vuol dire che ha una logica di valildazione,
 * */
public interface ValidatorValue {

    void validate() throws ValidationException;
}
