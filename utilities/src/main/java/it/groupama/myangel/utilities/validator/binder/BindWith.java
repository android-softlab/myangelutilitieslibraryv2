package it.groupama.myangel.utilities.validator.binder;

import android.support.annotation.IdRes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Francesco in 03/07/17.
 */

/**
 * Esempio di utilizzo:
 *
 *          LA CLASSE BEAN:
 *
 *          @BindMethod(set ="setTesto", get ="getTesto")
 *          public class Bean  {
 *           @BindWith(R.id.id_tv)
 *           private  String testo;
 *
 *           public Bean(String testo) {
 *           this.testo = testo;
 *           }
 *           public String getTesto() {
 *           return testo;
 *           }
 *
 *           public void setTesto(String testo) {
 *           this.testo = testo;
 *           }

 *
 * */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface BindWith {
    @IdRes int[] value();
}
