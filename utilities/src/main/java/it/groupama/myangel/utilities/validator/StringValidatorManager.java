package it.groupama.myangel.utilities.validator;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import it.groupama.myangel.utilities.java.ArraySetList;
import it.groupama.myangel.utilities.validator.exception.ValidationException;


/**
 * Ad ogni EditText può essere associato un "StringValidatorManager"
 * in pratica è una mappa <Identificator, Validator> che tiene traccia di tutti i validator che
 * devono essere associati a quella EditText.
 * Prevede metodi per aggiungere o rimuovere i validator
 * nonchè la possibilità di stabilire una priorità tra i validator inseriti.
 *
 * infine viene implementato il metodo validate, che controlla la validità di una stringa
 * tenendo conto di tutti i validator associati all'EditText.
 *
 *     */

public class StringValidatorManager {

    public static final int NO_PRIORITY = Integer.MAX_VALUE;
    public static final int MAX_PRIORITY = Integer.MIN_VALUE;

    private final Map<Identificator, Validator> map = new LinkedHashMap<Identificator, Validator>() {
        @Override
        public Validator put(Identificator key, Validator value) {
            if (containsKey(key)) {
                throw new IllegalStateException("Duplicated key "+ key);
            }

            return super.put(key, value);
        }

    };

    public StringValidatorManager addValidator(final String identificator, final Validator validator) {
        map.put(new Identificator(identificator), validator);
        return this;
    }

    public StringValidatorManager addValidator(final String identificator, final Validator validator, final int priority) {
        map.put(new Identificator(identificator, priority), validator);
        return this;
    }

    public StringValidatorManager removeValidator(final String identificator) {
        map.remove(new Identificator(identificator));
        return this;
    }

    public StringValidatorManager removeValidator(final Validator validator) {
        for (Identificator s : map.keySet()) {
            if (map.get(s).equals(validator)) {
                map.remove(s);
                break;
            }
        }

        return this;
    }


    public void validate(String value) throws ValidationException {
        List<Identificator> identificators = new ArraySetList<>(map.keySet());
        Collections.sort(identificators);

        for (Identificator identificator : identificators) {
            map.get(identificator).validate(value);
        }
    }

    public class Identificator implements Serializable, Comparable<Identificator> {
        private String identificator;
        private int priority = NO_PRIORITY;

        public Identificator(String identificator) {
            this.identificator = identificator;
        }

        public Identificator(String identificator, int priority) {
            this.identificator = identificator;
            this.priority = priority;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Identificator that = (Identificator) o;

            return priority == that.priority && priority != NO_PRIORITY;

        }

        @Override
        public int hashCode() {
            return priority;
        }

        @Override
        public int compareTo(@NonNull Identificator identificator) {
            return priority < identificator.priority ? -1 : 1;
        }

        @Override
        public String toString() {
            return "Identificator{" +
                    "identificator='" + identificator + '\'' +
                    ", priority=" + priority +
                    '}';
        }
    }
}
