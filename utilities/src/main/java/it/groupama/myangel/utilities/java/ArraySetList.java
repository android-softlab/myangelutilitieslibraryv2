package it.groupama.myangel.utilities.java;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import it.groupama.myangel.utilities.action.DataPresentAction;
import it.groupama.myangel.utilities.logger.Logger;

/**
 * Created by softlab on 14/07/2017.
 */


/**
 *
 * Created by Francesco on 14/06/2016.
 */
public class ArraySetList<E> extends ArrayList<E> {
    private static final Logger LOGGER = Logger.getIntance(ArraySetList.class);
    private int count;

    private ContainsStrategy containsStrategy = new DefaultContainsStrategy();


    public ArraySetList(int capacity) {
        super(capacity);
    }

    public ArraySetList() {
    }

    public ArraySetList(E[] es) {
        this(es != null ? Arrays.asList(es) : new ArraySetList<E>());
    }


    public ArraySetList(Collection<? extends E> collection) {
        super(collection);
    }

    /**
     * @param collection
     * @return {@code true} se è stato aggiunto almeno un elemento
     */
    @Override
    public boolean addAll(Collection<? extends E> collection) {
        boolean modify = false;
        for (E item : collection) {
            boolean add = add(item);
            if (!modify) {
                modify = add;
            }
        }

        return modify;
    }

    /**
     * @param collection
     * @return {@code true} se è stato aggiunto almeno un elemento
     */
    public boolean addAll(E[] collection) {
        boolean modify = false;
        for (E item : collection) {
            boolean add = add(item);
            if (!modify) {
                modify = add;
            }
        }

        return modify;
    }

//    @Override
//    public boolean addAll(int index, Collection<? extends E> collection) {
//        boolean modify = false;
//        for (E item : collection) {
//            modify = add(item);
//        }
//
//        return modify;
//    }

    /**
     * Prima di aggiungere l'oggetto controlla se non è già presente.
     *
     * @param object
     * @return se l'oggetto è stato aggiunto
     */
    @Override
    public boolean add(E object) {
        return !contains(object) && super.add(object);
    }

    @Override
    public void add(int index, E object) {
        if (!contains(object)) {
            super.add(index, object);
        }

    }

    /**
     * Se l'oggetto è già presente nella lista, viene sovrascritto
     *
     * @param object
     * @return
     */
    public boolean addOverride(E object) {
        boolean add = add(object);
        if (!add) {
            for (int i = 0; i < size(); i++) {
                E e = get(i);
                if (e.equals(object)) {
                    remove(object);
                    add(i, object);
                    break;
                }
            }
        }

        return true;
    }

    /**
     * Torna il valore progressivo al precedente. <br>
     * La chiamata al metodo {@code get(int)} non incrementa il valore
     *
     * @return Torna il valore progressivo al precedente.
     */
    public E get() {
        try {
            return super.get(count++);
        } catch (IndexOutOfBoundsException e) {
            count = 0;
            return super.get(count++);
        }
    }

    /**
     * Ritorna i primi {@code max} valori
     *
     * @param max
     * @return
     */
    public List<E> getFirstValue(int max) {
        List<E> listRest = new ArraySetList<>();

        for (int i = 0; i < max; i++) {
            try {
                listRest.add(get(i));
            } catch (IndexOutOfBoundsException e) {
                break;
            }
        }

        return listRest;
    }


    /**
     * Rimuove gli oggetti con una determinata condizione {@code comparator}.
     * </br>
     * Remove all items with determined condition {@code comparator}.
     *
     * @param comparator
     */
    public void removeItems(ComparatorRemove<E> comparator) {
        Iterator<E> iterator = iterator();
        while (iterator.hasNext()) {
            E next = iterator.next();
            if (comparator.remove(next)) {
                iterator.remove();
            }
        }
    }

    /**
     * Rimuove gli oggetti con una determinata condizione {@code comparator}.
     * </br>
     * Remove all items with determined condition {@code comparator}.
     * <p/>
     * TODO Non testato
     */
    public void removeItems() {
        Iterator<E> iterator = iterator();
        while (iterator.hasNext()) {
            try {
                ComparatorRemove next = (ComparatorRemove) iterator.next();
                //noinspection unchecked
                if (next.remove(next)) {
                    iterator.remove();
                }

            } catch (ClassCastException e) {
                LOGGER.error("Item not implement ComparatorRemove");
            }
        }
    }


    public void setContainsField(String fieldName) {
        containsStrategy = new FieldNameContainsStrategy(fieldName);
    }

    public void clearContainsField() {
        containsStrategy = new DefaultContainsStrategy();
    }

    @Override
    public boolean contains(Object object) {
        return containsStrategy.contains(object, this);
    }

    private boolean superContains(Object object) {
        return super.contains(object);
    }


    public interface ContainsStrategy extends Serializable {
        boolean contains(Object object, ArraySetList list);
    }

    private class DefaultContainsStrategy implements ContainsStrategy {

        @Override
        public boolean contains(Object object, ArraySetList list) {
            return list.superContains(object);
        }
    }

    public void setContainsStrategy(ContainsStrategy containsStrategy) {
        if (containsStrategy == null) {
            throw new IllegalStateException("The containsStrategy cannot be null");
        }

        this.containsStrategy = containsStrategy;
    }


    private class FieldNameContainsStrategy implements ContainsStrategy {
        private String fieldName;

        public FieldNameContainsStrategy(String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public boolean contains(Object object, ArraySetList list) {
            try {
                Field declaredFieldObject = object.getClass().getDeclaredField(fieldName);
                for (Object o : list) {

                    try {
                        Field declaredField = o.getClass().getDeclaredField(fieldName);
                        declaredField.setAccessible(true);
                        declaredFieldObject.setAccessible(true);

                        Object valueObject = declaredFieldObject.get(object);
                        Object valueO = declaredField.get(o);

                        if (valueO != null && valueO.equals(valueObject)) {
                            throw new DataPresentAction();
                        }

                    } catch (NoSuchFieldException e) {
                        LOGGER.warn("The object of the list not contains the field " + fieldName);
                        break;
                    } catch (IllegalAccessException e) {
                        LOGGER.warn("The object of the list not contains the field " + fieldName);
                    } catch (DataPresentAction dataPresentAction) {
                        return true;
                    }
                }
            } catch (NoSuchFieldException e) {
                LOGGER.warn("The object " + object + " not contains the field " + fieldName);
            }

            return false;
        }
    }

    public interface ComparatorRemove<E> {
        /**
         * Return true if the element must be removed, false otherwise
         *
         * @param e
         * @return
         */
        boolean remove(E e);
    }


    /**
     * Check if 'prefix' start with one of the values of this list
     * @param prefix
     * @return
     */
    public boolean containsReverseStartWith(String prefix) {
        if (prefix != null) {
            for (E value : this) {
                try {
                    String next = (String) value;
                    boolean b = prefix.startsWith(next);
                    if (b) {
                        return true;
                    }
                } catch (ClassCastException e) {
                    LOGGER.debug("The key is not a String");
                }
            }
        }

        return false;
    }

}