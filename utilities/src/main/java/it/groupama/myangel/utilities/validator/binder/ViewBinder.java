package it.groupama.myangel.utilities.validator.binder;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

import it.groupama.myangel.utilities.collectionUtils.CollectionUtils;
import it.groupama.myangel.utilities.java.ArraySetList;
import it.groupama.myangel.utilities.logger.Logger;
import it.groupama.myangel.utilities.validator.exception.ValidationException;


/**
 *  Collega la view con i bean.
 *  Prevede principalmente due metodi
 *  --> bind: "interpreta" i dati del bean per poi popolare la view.
 *  --> commit: "recupera" i dati dalla  view, per popolare le proprietà dell'ogetto passato
 *  Il costruttore prevede due parametri:
 *  La view,
 *  un Bean generico, che a secondo del metodo chiamato verrà utilizzato per popolare la view
 *  oppure verrà popolato a partire dai dati presenti sulla view.
 */


/**
 * Esempio di utilizzo:
 *
 *          NELL'ACTIVITY:
 *
 *          Bean bean = new Bean("testo da inserire");
 *          ViewBinder viewBinder = new ViewBinder(view,bean);
 *          viewBinder.bind();
 *
 *          LA CLASSE BEAN:
 *
 *          @BindMethod(set ="setTesto", get ="getTesto")
 *          public class Bean  {
 *           @BindWith(R.id.id_tv)
 *           private  String testo;
 *
 *           public Bean(String testo) {
 *           this.testo = testo;
 *           }
 *           public String getTesto() {
 *           return testo;
 *           }
 *
 *           public void setTesto(String testo) {
 *           this.testo = testo;
 *           }

 *
 * */

public class ViewBinder<T> {

    private final Logger logger = Logger.getIntance(getClass());

    private View view;
    private T bean;

    private boolean validateAll = true;

    public ViewBinder(View view, T bean) {
        this.view = view;
        this.bean = bean;
        logger.debug(bean);
    }
    /***
     * per ogni campo che compone la classe, se è presente l'annotazione " @BindWith(R.id...)", viene identificato nella view l'elemento con il corrispettivo id,
     * se nel bind è presente un metodo chiamato "set text", viene eseguito.
     * se questo metodo non è presente, va a leggere tra le annotazioni della classe bind se è presente l'annotazione @BindMethod(set ="setTesto", get ="getTesto")
     * ed esegue il metodo indicato nel set
     */

    //dati da oggetto alla view
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void bind() {
        for (Field field : bean.getClass().getDeclaredFields()) {
            if (Modifier.isPrivate(field.getModifiers())) {
                BindWith bindWith = field.getAnnotation(BindWith.class);
                if (bindWith != null) {
                    for (int id : bindWith.value()) {
                        View viewById = view.findViewById(id);
                        if (viewById != null) {
                            Class<? extends View> viewByIdClass = viewById.getClass();
                            try {

                                Method setText = getMethod(viewByIdClass, "setText", getTypes(field));
                                field.setAccessible(true);
                                setText.invoke(viewById, field.get(bean));

                            } catch (NoSuchMethodException e) {
                                cicrlarSetValue(viewByIdClass, viewById, field);

                            } catch (InvocationTargetException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }


                }
            }
        }

    }

    private Method getMethod(Class<?> classCheck, final String methodName, Class<?>... params) throws NoSuchMethodException {
        for (Class<?> param : params) {
            try {
                return classCheck.getDeclaredMethod(methodName, param);
            } catch (NoSuchMethodException e) {
                logger.errorMessage(e);
            }
        }

        throw new NoSuchMethodException(classCheck + " " + methodName + " " + CollectionUtils.toString(params));
    }

    private Class[] getTypes(final Field field) {
        if (field.getType().equals(String.class) || field.getType().equals(CharSequence.class)) {
            return new Class[] {String.class, CharSequence.class};
        }

        return new Class[] {field.getType()};
    }
    
    /**
     * per ogni campo che compone la classe, se è presente l'annotazione " @BindWith(R.id...)", viene identificato nella view l'elemento con il corrispettivo id,
     * se nel bind è presente un metodo chiamato "get text", viene eseguito, ed il valore letto salvato nel bind
     * se questo metodo non è presente, va a leggere tra le annotazioni della classe bind se è presente l'annotazione @BindMethod(set ="setTesto", get ="getTesto")
     * ed esegue il metodo indicato nel get
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public T commit() throws ValidationException {
        final List<ValidationException> list = new ArraySetList<>();
        for (Field field : bean.getClass().getDeclaredFields()) {
            if (Modifier.isPrivate(field.getModifiers())) {
                BindWith bindWith = field.getAnnotation(BindWith.class);
                if (bindWith != null) {
                    for (int id : bindWith.value()) {
                        View viewById = view.findViewById(id);
                        if (viewById != null) {

                            try {
                                ((ValidatorValue) viewById).validate();
                            } catch (ValidationException e) {
                                if (validateAll) {
                                    list.add(e);
                                    continue;
                                } else {
                                    throw e;
                                }

                            } catch (ClassCastException e) {
                                //
                            }

                            Class<? extends View> viewByIdClass = viewById.getClass();

                            try {
                                Method getText = viewByIdClass.getDeclaredMethod("getText");
                                setValue(field, getText.invoke(viewById));

                            } catch (NoSuchMethodException e) {
                                circularGetValue(viewByIdClass, viewById, field);
                            } catch (InvocationTargetException | IllegalAccessException e) {
                                e.printStackTrace();

                            }
                        }

                    }

                }
            }
        }

        if (!list.isEmpty()) {
            throw new ValidationException(list);
        }


        return bean;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void cicrlarSetValue(Class<?> viewByIdClass, final View viewById, final Field field) {
        if (!viewByIdClass.equals(Object.class)) {
            BindMethod bindMethod = viewByIdClass.getAnnotation(BindMethod.class);
            if (bindMethod != null) {
                try {

                    Method method = getMethod(viewByIdClass, bindMethod.set(), getTypes(field));
                    field.setAccessible(true);
                    method.invoke(viewById, field.get(bean));

                } catch (NoSuchMethodException e1) {
                    logger.errorMessage(e1);
                } catch (InvocationTargetException | IllegalAccessException e1) {
                    logger.error(e1);
                }
            } else {
                cicrlarSetValue(viewByIdClass.getSuperclass(), viewById, field);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void circularGetValue(Class<?> viewByIdClass, final View viewById, final Field field) {
        if (!viewByIdClass.equals(Object.class)) {
            BindMethod bindMethod = viewByIdClass.getAnnotation(BindMethod.class);
            if (bindMethod != null) {
                try {

                    Method method = viewByIdClass.getDeclaredMethod(bindMethod.get());
                    setValue(field, method.invoke(viewById));

                } catch (NoSuchMethodException e1) {
                    logger.errorMessage(e1);
                } catch (InvocationTargetException | IllegalAccessException e1) {
                    logger.error(e1);
                }
            } else {
                circularGetValue(viewByIdClass.getSuperclass(), viewById, field);
            }
        }
    }


    private void setValue(Field field, Object value) throws IllegalAccessException {
        if (value != null) {
            field.setAccessible(true);
            field.set(bean, value);
        }
    }

    public void setValidateAll(boolean validateAll) {
        this.validateAll = validateAll;
    }
}
