package it.groupama.myangel.utilities.action;

/**
 * Created by softlab on 14/07/2017.
 */

/**
 * Created by Francesco in 02/09/16.
 */
public class FoundDataAction extends Exception {
    private Object data;

    public FoundDataAction() {
    }

    public FoundDataAction(Object data) {
        this.data = data;
    }

    @SuppressWarnings("unchecked")
    public <T> T getData() {
        return (T) data;
    }
}