package it.groupama.myangel.utilities.validator;


import it.groupama.myangel.utilities.validator.exception.ValidationException;

/**
 * Created by Francesco in 23/06/17.
 */


public class NotEmptyValidator implements Validator {

    @Override
    public void validate(String value) throws ValidationException {
        if (value == null || "".equals(value)) {
            throw new ValidationException("Campo obbligatorio");
        }
    }
}
