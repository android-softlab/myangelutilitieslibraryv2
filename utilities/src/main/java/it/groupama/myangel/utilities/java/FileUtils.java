package it.groupama.myangel.utilities.java;

import java.io.File;

/**
 * Created by Francesco in 28/04/17.
 */

public abstract class FileUtils {
    protected FileUtils() {
    }


    public static String getNameWithoutExtension(File file) {
        String name = file.getName();
        return name.substring(0, name.lastIndexOf("."));
    }
}
