package it.groupama.myangel.utilities.java;

import java.text.DecimalFormat;

/**
 * Created by Francesco in 05/05/17.
 */

public class Chronometer {

    private final DecimalFormat decimalFormat = new DecimalFormat("0,000");

    private static final String UNIT = " s";

    private long startTime;
    private String finalTime;

    public Chronometer(boolean autoStart) {
        if (autoStart) {
            start();
        }
    }

    public void start() {
        startTime = System.currentTimeMillis();
        finalTime = "";
    }

    public String stop() {
        final long fine = System.currentTimeMillis();
        long diff = fine - startTime;

        finalTime = decimalFormat.format(diff);
        return finalTime;
    }

    public String getTime() {
        return finalTime;
    }

    public String getTimeWithUnit() {
        return finalTime + UNIT;
    }

}
