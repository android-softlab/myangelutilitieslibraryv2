package it.groupama.myangel.utilities.collectionUtils;

/**
 * Created by softlab on 14/07/2017.
 */

import java.util.Collection;
import java.util.Map;

/**
 * Raccolta di metodi per analizzare/elaborare i dati presenti in liste, mappe, array di object
 * */
public class CollectionUtils {


    public static boolean isEmpty(Collection list) {
        return list == null || list.isEmpty();
    }

    public static boolean isEmpty(Map map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNotEmpty(Collection list) {
        return !isEmpty(list);
    }

    public static boolean isNotEmpty(Map map) {
        return !isEmpty(map);
    }

    public static String toString(Object[] array) {
        final StringBuilder stringBuilder = new StringBuilder();
        if (array != null && array.length > 0) {

            for (Object o : array) {
                stringBuilder.append(o).append("\n");
            }


            stringBuilder.replace(stringBuilder.lastIndexOf("\n"), stringBuilder.length(), "");
        }

        return stringBuilder.toString();
    }

    public static boolean isEmpty(Object[] params) {
        return params == null || params.length == 0;
    }

    public static boolean isNotEmpty(Object[] params) {
        return !isEmpty(params);
    }
}