package it.groupama.myangel.utilities.java;

import java.util.HashMap;
import java.util.List;

/**
 *
 * Created by Francesco on 15/06/2016.
 */
public class HashMapValueList<K, VI> extends HashMap<K, List<VI>> {

    /**
     *
     * @param key
     * @param value
     * @return ritorna la lista aggiornata
     */
    public List<VI> putValue(K key, VI value) {

        List<VI> vouchers = get(key);
        if (vouchers == null) {
            vouchers = new ArraySetList<>();
            super.put(key, vouchers);
        }

        vouchers.add((VI) value);
        return vouchers;
    }

    /**
     *
     * @param key
     * @param value
     * @return ritorna la lista aggiornata
     */
    public List<VI> putAllValue(K key, List<VI> value) {

        List<VI> vouchers = get(key);
        if (vouchers == null) {
            vouchers = new ArraySetList<>();
            super.put(key, vouchers);
        }

        vouchers.addAll(value);
        return vouchers;
    }


    public List<VI> getAllValue() {
        List<VI> list = new ArraySetList<>();
        for (List<VI> items : values()) {
            list.addAll(items);
        }

        return list;
    }


    /**
     * Controlla se in almeno una lista del value di questa mappa, è presente l'oggetto passato
     */
    public boolean containsItemValue(VI value) {
        for (List<VI> list : values()) {
            //noinspection unchecked
            if (list.contains((VI)value)) {
                return true;
            }
        }

        return false;
    }
}
