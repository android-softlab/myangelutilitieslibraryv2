package it.groupama.myangel.utilities.validator.exception;

import java.util.List;

import it.groupama.myangel.utilities.java.ArraySetList;


/**
 * Created by Francesco in 23/06/17.
 */

public class ValidationException extends Exception {

    private List<ValidationException> list = new ArraySetList<>();


    public ValidationException(List<ValidationException> list) {
        this.list = list;
    }

//    public ValidationException() {
//    }

    public ValidationException(String message) {
        super(message);
    }

//    public ValidationException(String message, Throwable cause) {
//        super(message, cause);
//    }
//
//    public ValidationException(Throwable cause) {
//        super(cause);
//    }
//
//    public ValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
//        super(message, cause, enableSuppression, writableStackTrace);
//    }

    public void throwThis() throws ValidationException {
        if (!list.isEmpty()) {
            throw this;
        }
    }
}
